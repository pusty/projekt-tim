<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 06.04.2018
 * Time: 10:40
 */

namespace App\Controller;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;

use App\Service\CarService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CarController extends Controller {
    /**
     * @Route("/car/class/add", name="car_class_add")
     * @Method({"POST"})
     */
    public function AddClass(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $carService = new CarService($em);
        $tab['error'] = $response->getMessage(802);
        try {
            $carService->addClass($request);
        } catch (UsersExceptions $e){
            $tab['error'] = $e->getMessage();
        }
        $response = new JsonResponse($tab,Response::HTTP_OK);
        return $response;

    }
    /**
     * @Route("/car/type/add", name="car_type_add")
     * @Method({"POST"})
     */
    public function AddType(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $carService = new CarService($em);
        $tab['error'] = $response->getMessage(802);
        try {
            $carService->addType($request);
        } catch (UsersExceptions $e){
            $tab['error'] = $e->getMessage();
        }
        $response = new JsonResponse($tab,Response::HTTP_OK);
        return $response;

    }
    /**
     * @Route("/car/add", name="car_add")
     * @Method({"POST"})
     */
    public function AddCar(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $carService = new CarService($em);
        $tab['error'] = $response->getMessage(802);
        try {
            $carService->addClass($request);
        } catch (UsersExceptions $e){
            $tab['error'] = $e->getMessage();
        }
        $response = new JsonResponse($tab,Response::HTTP_OK);
        return $response;

    }
}