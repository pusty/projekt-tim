<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 06.04.2018
 * Time: 10:49
 */

namespace App\Controller;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use App\Service\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class OrderController extends Controller {
    /**
     * @Route("/order/status/add", name="order_status_add")
     * @Method({"POST"})
     */
    public function AddStatus(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['error'] = $response->getMessage(802);
        try {
            $orderService->addStatus($request);
        } catch (UsersExceptions $e){
            $tab['error'] = $e->getMessage();
        }
        $response = new JsonResponse($tab,Response::HTTP_OK);
        return $response;

    }
    /**
     * @Route("/order/add", name="order_add")
     * @Method({"POST"})
     */
    public function AddOrder(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['error'] = $response->getMessage(802);
        try {
            $orderService->addOrder($request);
        } catch (UsersExceptions $e){
            $tab['error'] = $e->getMessage();
        }
        $response = new JsonResponse($tab,Response::HTTP_OK);
        return $response;

    }


}