<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 28.03.2018
 * Time: 23:36
 */

namespace App\Exceptions;


class ResponseErrors
{
    private $messages = array(
        801 => 'NOT_VALID_REQUEST',
        802 => 'SUCCESS',
        803 => 'UNKNOWN_ERROR',

        810 => 'NO_USERNAME',
        811 => 'NO_PASSWORD',
        812 => 'NO_EMAIL',
        813 => 'NO_PRIVILEGES',
        814 => 'USER_ALREADY_EXISTS',
        815 => 'LOGIN_ERROR',

        820 => 'NO_CODE',
        821 => 'NO_DESCRIPTION',
        822 => 'DICTIONARY_ERROR',
        823 => 'PERMISSIONS_ERROR',

        830 => 'NO_PRODUCTION_YEAR',
        831 => 'NO_FUEL_TYPE',
        832 => 'NO_ENGINE_CAPACITY',
        833 => 'NO_TRAMSISSION_TYPE',
        834 => 'NO_SEATS_NUMBER',
        835 => 'NO_CAR_TYPE',
        836 => 'NO_CAR_CLASS',

        870 => 'NO_START_DATE',
        871 => 'NO_END_DATE',
        872 => 'NO_ORDER_STATUS',
        873 => 'NO_CAR',
        874 => 'ORDER_ERROR'




    );

    /**
     * @return array
     */
    public function getMessage($message) {
        return $this->messages[$message];
    }
}