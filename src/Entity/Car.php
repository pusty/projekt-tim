<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 04.04.2018
 * Time: 11:19
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="api_Cars")
 */

class Car
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToOne(targetEntity="CarType")
     * @ORM\JoinColumn(name="carTypeId", referencedColumnName="id")
     */
    private $carType;
    /**
     * @ORM\OneToOne(targetEntity="CarClass")
     * @ORM\JoinColumn(name="carClassId", referencedColumnName="id")
     */
    private $carClass;
    /**
     * @ORM\Column(type="integer", length=4)
     */
    private $productionYear;
    /**
     * @ORM\Column(type="string", length=4, columnDefinition="ENUM('ON', 'PB', 'H')")
     */
    private $fuel;
    /**
     * @ORM\Column(type="integer", length=5)
     */
    private $engineCapacity;
    /**
     * @ORM\Column(type="string", length=20, columnDefinition="ENUM('MANUALNA', 'AUTOMATYCZNA', 'BRAK')")
     */
    private $tramsission;
    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $seats;
    /**
     * @ORM\Column(type="boolean", options={"default"=true})
     */
    private $active;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCarType()
    {
        return $this->carType;
    }

    /**
     * @param mixed $carType
     */
    public function setCarType($carType): void
    {
        $this->carType = $carType;
    }

    /**
     * @return mixed
     */
    public function getCarClass()
    {
        return $this->carClass;
    }

    /**
     * @param mixed $carClass
     */
    public function setCarClass($carClass): void
    {
        $this->carClass = $carClass;
    }

    /**
     * @return mixed
     */
    public function getProductionYear()
    {
        return $this->productionYear;
    }

    /**
     * @param mixed $productionYear
     */
    public function setProductionYear($productionYear): void
    {
        $this->productionYear = $productionYear;
    }

    /**
     * @return mixed
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @param mixed $fuel
     */
    public function setFuel($fuel): void
    {
        $this->fuel = $fuel;
    }

    /**
     * @return mixed
     */
    public function getEngineCapacity()
    {
        return $this->engineCapacity;
    }

    /**
     * @param mixed $engineCapacity
     */
    public function setEngineCapacity($engineCapacity): void
    {
        $this->engineCapacity = $engineCapacity;
    }

    /**
     * @return mixed
     */
    public function getTramsission()
    {
        return $this->tramsission;
    }

    /**
     * @param mixed $tramsission
     */
    public function setTramsission($tramsission): void
    {
        $this->tramsission = $tramsission;
    }

    /**
     * @return mixed
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param mixed $seats
     */
    public function setSeats($seats): void
    {
        $this->seats = $seats;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }
}