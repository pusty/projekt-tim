<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 06.04.2018
 * Time: 10:51
 */

namespace App\Service;

use App\Entity\Car;
use App\Entity\Order;
use App\Entity\OrderStatus;
use App\Entity\Users;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class OrderService
{
    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function addStatus(Request $request)
    {
        $response = new ResponseErrors();
        $order = new OrderStatus();
        if ($request->get('code') != null) {
            $order->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if ($request->get('description') != null) {
            $order->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(OrderStatus::class)->findOneBy(['code' => $request->get('code')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($order);
            $em->flush();
            if (null == $order->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addOrder(Request $request){
        $response = new ResponseErrors();
        $order = new Order();
        if ($request->get('start') != null) {
            $order->setDateFrom($request->get('start'));
        } else throw new UsersExceptions($response->getMessage(870));
        if ($request->get('end') != null) {
            $order->setDateFrom($request->get('end'));
        } else throw new UsersExceptions($response->getMessage(871));
        if ($request->get('status') != null) {
            $order->setDateFrom($this->doctrine->getRepository(OrderStatus::class)->findOneBy(['code' => $request->get('status')]));
        } else throw new UsersExceptions($response->getMessage(872));
        if ($request->get('user') != null) {
            $order->setDateFrom($this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('user')]));
        } else throw new UsersExceptions($response->getMessage(810));
        if ($request->get('car') != null) {
            $order->setDateFrom($this->doctrine->getRepository(Car::class)->findOneBy(['id' => $request->get('car')]));
        } else throw new UsersExceptions($response->getMessage(873));
        $check = $this->doctrine->getRepository(OrderStatus::class)->findOneBy([
            'dateFrom' => $request->get('start'),
            'dateTo' => $request->get('end'),
            'userId' => $request->get('user')
        ]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($order);
            $em->flush();
            if (null == $order->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(874));
    }
}