<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 06.04.2018
 * Time: 10:42
 */

namespace App\Service;

use App\Entity\Car;
use App\Entity\CarClass;
use App\Entity\CarType;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class CarService
{
    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function addClass(Request $request)
    {
        $response = new ResponseErrors();
        $car = new CarClass();
        if ($request->get('code') != null) {
            $car->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if ($request->get('description') != null) {
            $car->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(CarClass::class)->findOneBy(['code' => $request->get('code')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($car);
            $em->flush();
            if (null == $car->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addType(Request $request)
    {
        $response = new ResponseErrors();
        $car = new CarType();
        if ($request->get('code') != null) {
            $car->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if ($request->get('description') != null) {
            $car->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(CarType::class)->findOneBy(['code' => $request->get('code')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($car);
            $em->flush();
            if (null == $car->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addCar(Request $request)
    {
        $response = new ResponseErrors();
        $car = new Car();
        if ($request->get('year') != null) {
            $car->setProductionYear($request->get('year'));
        } else throw new UsersExceptions($response->getMessage(830));
        if ($request->get('fuel') != null) {
            $car->setFuel($request->get('fuel'));
        } else throw new UsersExceptions($response->getMessage(831));
        if($request->get('engine') != null){
            $car->setEngineCapacity($request->get('engine'));
        } else throw new UsersExceptions($response->getMessage(832));
        if($request->get('transmission') != null){
            $car->setEngineCapacity($request->get('transmission'));
        } else throw new UsersExceptions($response->getMessage(833));
        if($request->get('seats') != null){
            $car->setSeats($request->get('seats'));
        } else throw new UsersExceptions($response->getMessage(834));
        $car->setActive(true);
        if($request->get('type') != null){
            $car->setCarType($this->doctrine->getRepository(CarType::class)->findOneBy(['code' => $request->get('type')]));
        } else throw new UsersExceptions($response->getMessage(835));
        if($request->get('class') != null){
            $car->setCarClass($this->doctrine->getRepository(CarClass::class)->findOneBy(['code' => $request->get('class')]));
        } else throw new UsersExceptions($response->getMessage(836));
        $em = $this->doctrine->getManager();
        $em->persist($car);
        $em->flush();
        if (null == $car->getId()) {
           throw new UsersExceptions($response->getMessage(803));
        }
    }

}