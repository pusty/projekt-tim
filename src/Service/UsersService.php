<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 28.03.2018
 * Time: 22:23
 */

namespace App\Service;

use App\Entity\Permissions;
use App\Entity\UserPerms;
use App\Entity\Users;
use App\Entity\UserStatus;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class UsersService
{

    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function saveUser(Request $request)
    {
        $response = new ResponseErrors();
        $user = new Users();
        if ($request->get('login') != null) {
            $user->setLogin($request->get('login'));
        } else throw new UsersExceptions($response->getMessage(810));
        if ($request->get('password') != null) {
            $user->setPassword(md5($request->get('password')));
        } else throw new UsersExceptions($response->getMessage(811));
        if ($request->get('email') != null) {
            $user->setEmail($request->get('email'));
        } else throw new UsersExceptions($response->getMessage(812));
        $user->setActive(true);
        $user->setAction($this->doctrine->getRepository(UserStatus::class)->findOneBy(['code' => $request->get('status')]));
        $check = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
            if (null == $user->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(814));

    }

    public function loginUser(Request $request)
    {
        $response = new ResponseErrors();
        if ($request->get('login') == null) throw new UsersExceptions($response->getMessage(810));
        if ($request->get('password') == null) throw new UsersExceptions($response->getMessage(811));
        $user = $this->doctrine->getRepository(Users::class)->findOneBy([
                'login' => $request->get('login'),
                'password' => md5($request->get('password'))]
        );
        if (!$user) throw new UsersExceptions($response->getMessage(815));

    }

    public function addStatus(Request $request){
        $response = new ResponseErrors();
        $user = new UserStatus();
        if($request->get('code') != null){
            $user->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if($request->get('description') != null){
            $user->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(UserStatus::class)->findOneBy(['code' => $request->get('code')]);
        if(!$check){
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
            if(null == $user->getId()){
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addPermission(Request $request){
        $response = new ResponseErrors();
        $user = new Permissions();
        if($request->get('code') != null){
            $user->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if($request->get('description') != null){
            $user->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(Permissions::class)->findOneBy(['code' => $request->get('code')]);
        if(!$check){
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
            if(null == $user->getId()){
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }
    public function addUserPermission(Request $request){
        $response = new ResponseErrors();
        $user = new UserPerms();
        if($request->get('login') != null){
            $user->setUserId($this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]));
        } else throw new UsersExceptions($response->getMessage(810));
        if($request->get('permission') != null){
            $user->setPermissionId($this->doctrine->getRepository(Permissions::class)->findOneBy(['code' => $request->get('permission')]));
        } else throw new UsersExceptions($response->getMessage(813));
        $check = $this->doctrine->getRepository(UserPerms::class)->findOneBy(['userId' => $request->get('login'), 'permissionId' => $request->get('permission')]);
        if(!$check){
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
            if(null == $user->getId()){
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(823));
    }
}